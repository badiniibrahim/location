import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

window.matchMedia =
    (window.matchMedia as any) ||
    function() {
        return {
            matches: false,
            addListener() {},
            removeListener() {},
        };
    };

window.requestAnimationFrame =
    (window.requestAnimationFrame as any) ||
    function(callback) {
        setTimeout(callback, 0);
    };

const localStorageMock = {
    getItem: jest.fn(),
    setItem: jest.fn(),
    clear: jest.fn(),
};

(global as any).localStorage = localStorageMock;
