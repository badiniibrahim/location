import { getFormattedDate, computeTotalDistance } from './helpers';
import { TLocation } from 'Models';

describe('helpers Tests', () => {
    it('should return correct date', () => {
        const timestamp: string = '2020-05-06T12:39:41.744Z';
        expect(getFormattedDate(timestamp)).toEqual('06/05/2020');
    });

    it('should return the total distance', () => {
        const locations: TLocation[] = [
            {
                coords: {
                    longitude: 2.11779679001605,
                    latitude: 48.9089599277175,
                },
                timestamp: '2020-05-06T11:53:40.072Z',
            },
            {
                coords: {
                    longitude: 2.11799906966679,
                    latitude: 48.908967647342,
                },
                timestamp: '2020-05-06T11:53:40.998Z',
            },
        ];

        expect(computeTotalDistance(locations)).toEqual(15);
    });
});
