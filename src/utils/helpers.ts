import _ from 'lodash';
import { distanceTo } from 'geolocation-utils';
import { TLocation } from 'Models';

const pad = (s: number) => {
    return s < 10 ? `0${s}` : s;
};

const getFormattedDate = (date: string) => {
    const d = new Date(date);
    return d
        ? [pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear()].join('/')
        : undefined;
};

function computeTotalDistance(locations: TLocation[]): number {
    let totaldistance: number = 0;
    for (const i of _.range(0, locations.length - 1)) {
        totaldistance += distanceTo(
            locations[i].coords,
            locations[i + 1].coords,
        );
    }
    return Math.ceil(totaldistance);
}

export { getFormattedDate, computeTotalDistance };
