import React from 'react';
import { TRoute } from 'Models';
import CustomText from '../CustomText/CustomText';
import wording from '../../utils/wording.json';
import { getFormattedDate, computeTotalDistance } from '../../utils/helpers';

type Props = { routes: TRoute };

const LocationsItems: React.FC<Props> = routes => {
    const ListItems = routes.routes.locations.map((item, index) => {
        return (
            <React.Fragment key={index}>
                <div className="locations-items" key={index}>
                    <CustomText
                        textValue={`${wording.routes.latitude}${item.coords.latitude}`}
                    />
                    <CustomText
                        textValue={`${wording.routes.longitude}${item.coords.longitude}`}
                    />
                    <CustomText
                        textValue={`${
                            wording.routes.timestamp
                        }${getFormattedDate(item.timestamp)}`}
                    />
                </div>
            </React.Fragment>
        );
    });
    return (
        <div>
            <CustomText
                textValue={`${
                    wording.routes.theTotalDistance
                }${computeTotalDistance(routes.routes.locations)}${' m'}`}
            />
            {ListItems}
        </div>
    );
};

export default LocationsItems;
