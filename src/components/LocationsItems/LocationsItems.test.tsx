import React from 'react';
import { shallow } from 'enzyme';
import LocationsItems from './LocationsItems';

describe('LocationsItems Tests', () => {
    let props: any;
    beforeAll(() => {
        props = {
            routes: {
                locations: [
                    {
                        coords: {
                            latitude: 0,
                            longitude: 0,
                        },
                        timestamp: '',
                    },
                ],
            },
        };
    });

    it('Renders without crashing', () => {
        const wrapper = shallow(<LocationsItems {...props} />);
        expect(wrapper.find('div').exists()).toBe(true);
    });
});
