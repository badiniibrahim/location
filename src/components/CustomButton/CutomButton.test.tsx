import React from 'react';
import { shallow } from 'enzyme';
import CustomButton from './CustomButton';

describe('CustomButton Test', () => {
    it('renders with title', () => {
        const props = { title: 'My Custom Button', onClick: () => {} };
        const wrapper = shallow(<CustomButton {...props} />);
        expect(wrapper.find('button').exists()).toBe(true);
        expect(wrapper.find('button').text()).toBe('My Custom Button');
    });
});
