import React, { useState } from 'react';
import { TRoute } from 'Models';
import CustomText from '../CustomText';
import CustomButton from '../CustomButton';
import wording from '../../utils/wording.json';
import { getFormattedDate } from '../../utils/helpers';
import LocationsItems from '../../components/LocationsItems';

type Props = { routes: TRoute[] };

const RoutesItems: React.FC<Props> = ({ routes }) => {
    const [open, setOpen] = useState<boolean>(false);
    const [current, setCurrent] = useState<number>(-1);

    const ListItems = routes.map((item, index) => {
        return (
            <React.Fragment key={index}>
                <div className="routes-items" key={index}>
                    <div className="routes-items__content">
                        <CustomText textValue={item.id} />
                        <CustomText textValue={item.activity as string} />
                        <CustomText
                            textValue={
                                getFormattedDate(item.timestamp) as string
                            }
                        />
                        <CustomButton
                            title={wording.routes.btnText}
                            onClick={() => {
                                setOpen(!open);
                                setCurrent(index);
                            }}
                        />
                    </div>
                    {open && current === index && (
                        <LocationsItems routes={item} />
                    )}
                </div>
            </React.Fragment>
        );
    });
    return <div>{ListItems}</div>;
};

export default RoutesItems;
