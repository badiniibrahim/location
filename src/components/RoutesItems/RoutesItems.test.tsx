import React from 'react';
import { shallow } from 'enzyme';
import RoutesItems from './RoutesItems';

describe('RoutesItems Tests', () => {
    let props: any;
    beforeAll(() => {
        props = {
            routes: [],
        };
    });

    it('Renders without crashing', () => {
        const wrapper = shallow(<RoutesItems {...props} />);
        expect(wrapper.find('div')).toHaveLength(1);
    });
});
