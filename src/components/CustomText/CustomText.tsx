import React from 'react';

type Props = { textValue: string | number };

const CustomText: React.FC<Props> = ({ textValue }) => {
    return <p>{textValue}</p>;
};

export default CustomText;
