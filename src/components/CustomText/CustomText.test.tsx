import React from 'react';
import { shallow } from 'enzyme';
import CustomText from './CustomText';

describe('CustomText Test', () => {
    it('renders with textValue', () => {
        const props = { textValue: 'My Custom Text' };
        const wrapper = shallow(<CustomText {...props} />);
        expect(wrapper.find('p').exists()).toBe(true);
        expect(wrapper.find('p').text()).toBe('My Custom Text');
    });
});
