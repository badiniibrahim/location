import { combineReducers } from 'redux';
import locationReducer from '../_reducers/location.reducer';

const rootReducer = combineReducers({
    locationReducer,
});

export default rootReducer;
