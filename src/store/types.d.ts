/// <reference types="types.d.ts" />

import { RouterAction, LocationChangeAction } from 'react-router-redux';
import { LocationActions } from '../_actions/location.actions';

type ReactRouterAction = RouterAction | LocationChangeAction;

export type RootState = StateType<typeof import('./root-reducer').default>;

export type RootAction = LocationActions;
