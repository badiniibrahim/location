import React from 'react';
import { mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import Main from './Main';

configure({ adapter: new Adapter() });

describe('Main Test Suite', () => {
    let wrapper;
    let store: any;

    beforeAll(() => {
        const middlewares = [thunk];
        const mockStore = configureStore(middlewares);
        store = mockStore({
            locationReducer: {
                routes: [
                    {
                        id: '',
                        activity: '',
                        timestamp: '',
                        locations: [],
                    },
                ],
            },
        });
    });

    it('should render Main', () => {
        wrapper = mount(
            <Provider store={store}>
                <Main />
            </Provider>,
        );
        expect(wrapper.find('.app')).toHaveLength(1);
    });
});
