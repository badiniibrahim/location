import React from 'react';
import { connect } from 'react-redux';
import { RootState } from '../store/types';
import { saveLocationData } from '../_actions/location.actions';
import RoutesItems from '../components/RoutesItems';

import data from '../assets/data.json';

const mapStateToProps = (state: RootState) => ({
    routes: state.locationReducer.routes,
});

const dispatchProps = { saveLocationData };

type Props = {} & ReturnType<typeof mapStateToProps> & typeof dispatchProps;

class Main extends React.PureComponent<Props, Props> {
    componentDidMount() {
        data.routes.map(item => {
            this.props.saveLocationData({
                id: item.id,
                activity: item.activity,
                timestamp: item.timestamp,
                locations: item.locations,
            });
        });
    }

    render() {
        return (
            <div className="app">
                <div className="app__content">
                    <RoutesItems routes={this.props.routes} />
                </div>
            </div>
        );
    }
}

export default connect(mapStateToProps, dispatchProps)(Main);
