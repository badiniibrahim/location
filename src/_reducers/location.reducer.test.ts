import { getType } from 'typesafe-actions';
import * as actions from '../_actions/location.actions';
import reducer from './location.reducer';
import { TRoute, TRoutes } from 'Models';

describe('location.reducer Tests', () => {
    it('should check saveLocationData success', () => {
        const payload: TRoute = {
            id: '287631248083171e9d577634',
            activity: 'in_vehicle',
            timestamp: '2020-05-06T12:39:41.744Z',
            locations: [
                {
                    coords: {
                        latitude: 2.11779679001605,
                        longitude: 48.9089599277175,
                    },
                    timestamp: '2020-05-06T11:53:40.072Z',
                },
            ],
        };

        const action = {
            type: getType(actions.saveLocationData),
            payload,
        };

        const expectedState: TRoutes = {
            routes: [
                {
                    id: payload.id,
                    activity: payload.activity,
                    timestamp: payload.timestamp,
                    locations: payload.locations,
                },
            ],
        };

        expect(reducer(undefined, action)).toEqual(expectedState);
    });
});
