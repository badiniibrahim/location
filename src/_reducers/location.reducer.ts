import { Reducer } from 'redux';
import { getType } from 'typesafe-actions';
import * as actions from '../_actions/location.actions';
import { TRoutes } from 'Models';

export type LocationState = Readonly<{
    routes: TRoutes[];
}>;

const initialState = { routes: [] };

type Actions = actions.LocationAction;

const locationDataReducer: Reducer<TRoutes, Actions> = (
    state = initialState,
    action: Actions,
) => {
    switch (action.type) {
        case getType(actions.saveLocationData): {
            return {
                ...state,
                routes: [...state.routes, action.payload],
            };
        }
        default:
            return { ...state };
    }
};

export default locationDataReducer;
