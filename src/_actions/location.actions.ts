import { SAVE_LOCATION_DATA } from './actionsTypes';

import { ActionType, createAction } from 'typesafe-actions';
import { TRoute } from 'Models';

const saveLocationData = createAction(SAVE_LOCATION_DATA)<TRoute>();

export type LocationAction = ActionType<typeof saveLocationData>;

export { saveLocationData };
