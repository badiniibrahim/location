/// <reference types="react-scripts" />

declare module 'Models' {
    export type TRoutes = {
        routes: TRoute[];
    };

    export type TRoute = {
        id: string;
        timestamp: string;
        activity: string;
        locations: TLocation[];
    };

    export type TLocation = {
        coords: TCoords;
        timestamp: string;
    };

    export type TCoords = {
        latitude: number;
        longitude: number;
    };
}
